package com.bigfans.orderservice.service;

import com.bigfans.framework.dao.BaseService;
import com.bigfans.framework.model.PageBean;
import com.bigfans.orderservice.model.Invoice;
import com.bigfans.orderservice.model.Order;
import com.bigfans.orderservice.model.OrderItem;

import java.util.List;

/**
 * 
 * @Description: 订单服务类
 * @author lichong 2014年12月5日上午11:24:24
 *
 */
public interface OrderService extends BaseService<Order> {

	PageBean<Order> pageByUser(String userId, Long start, Long pagesize) throws Exception;

	Order getOrderByUser(String userId, String orderId) throws Exception;

	int cancel(String userId, String orderId) throws Exception;

	int updateStatusToPaid(String orderId) throws Exception;

	void create(Order e , List<OrderItem> items , Invoice invoice) throws Exception;
}
