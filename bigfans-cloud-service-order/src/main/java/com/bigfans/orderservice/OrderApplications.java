package com.bigfans.orderservice;

import com.bigfans.framework.Applications;
import com.bigfans.framework.CurrentUser;
import lombok.Data;

/**
 * @author lichong
 * @create 2018-04-27 下午9:00
 **/
public class OrderApplications extends Applications {

    private static String functionalUserToken;
    private static CurrentUser functionalUser;

    public static void setFunctionalUser(CurrentUser functionalUser) {
        OrderApplications.functionalUser = functionalUser;
    }

    public static CurrentUser getFunctionalUser() {
        return functionalUser;
    }

    public static void setFunctionalUserToken(String functionalUserToken) {
        OrderApplications.functionalUserToken = functionalUserToken;
    }

    public static String getFunctionalUserToken() {
        return functionalUserToken;
    }
}
