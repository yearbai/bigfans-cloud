package com.bigfans.cartservice.exception;

import com.bigfans.framework.exception.ServiceRuntimeException;

/**
 * 
 * @Title: 
 * @Description: 商品下架异常
 * @author lichong 
 * @date 2016年3月6日 下午1:14:05 
 * @version V1.0
 */
public class ProductOffSaleException extends ServiceRuntimeException {

	private static final long serialVersionUID = -9159121130020582475L;

	public ProductOffSaleException() {
	}
	
	public ProductOffSaleException(Throwable e) {
		super(e);
	}
	
	public ProductOffSaleException(String message) {
		super(message);
	}
}
