package com.bigfans.catalogservice.dao.impl;

import java.util.List;

import com.bigfans.catalogservice.dao.SpecOptionDAO;
import com.bigfans.catalogservice.model.SpecOption;
import com.bigfans.framework.cache.Cacheable;
import com.bigfans.framework.dao.MybatisDAOImpl;
import com.bigfans.framework.dao.ParameterMap;
import org.springframework.stereotype.Repository;

/**
 * 
 * @Description:
 * @author lichong
 * 2015年5月31日下午9:11:57
 *
 */
@Repository(SpecOptionDAOImpl.BEAN_NAME)
public class SpecOptionDAOImpl extends MybatisDAOImpl<SpecOption> implements SpecOptionDAO {
	
	public static final String BEAN_NAME = "specOptionDAO";

	@Override
	public List<SpecOption> listByCatId(String catId, Long start , Long pagesize)  {
		ParameterMap params = new ParameterMap(start , pagesize);
		params.put("categoryId", catId);
		return getSqlSession().selectList(className + ".list", params);
	}
	
	@Override
	public List<SpecOption> listByCatIds(List<String> catIds, Long start, Long pagesize)  {
		ParameterMap params = new ParameterMap(start , pagesize);
		params.put("categoryIds", catIds);
		return getSqlSession().selectList(className + ".list", params);
	}

	@Override
	public List<SpecOption> listByPgId(String pgId, Long start , Long pagesize)  {
		ParameterMap params = new ParameterMap(start , pagesize);
		params.put("pgId", pgId);
		return getSqlSession().selectList(className + ".list", params);
	}

	@Override
	public List<SpecOption> listByPid(String pid, Long start , Long pagesize)  {
		ParameterMap params = new ParameterMap(start , pagesize);
		params.put("pid", pid);
		return getSqlSession().selectList(className + ".list", params);
	}

	@Override
	public List<SpecOption> listByIdList(List<String> idList) {
		ParameterMap params = new ParameterMap();
		params.put("idList", idList);
		return getSqlSession().selectList(className + ".list", params);
	}

	@Override
	public List<SpecOption> list(String catId, String pgId, String pid, Long start, Long pagesize) {
		ParameterMap params = new ParameterMap(start , pagesize);
		params.put("categoryId", catId);
		params.put("pgId", pgId);
		params.put("pid", pid);
		return getSqlSession().selectList(className + ".list", params);
	}
}
