package com.bigfans.catalogservice.dao;

import com.bigfans.catalogservice.model.Product;
import com.bigfans.framework.dao.BaseDAO;
import com.bigfans.framework.model.PageBean;

import java.util.List;

public interface ProductDAO extends BaseDAO<Product>{

	List<Product> listByPgId(String pgId);
	
	List<Product> listByCategory(String catId, Long start, Long pagesize);

	List<Product> listByCategory(String[] catIds, Long start, Long pagesize);

	List<Product> listHotSale(Long start, Long pagesize);

	void updateHits(String productId);

	int updateThumb(String prodId, String thumbPath);

	void viewed(String productId, String userId);

	PageBean<Product> search(Product params, Long start, Long pagesize);
	
}
