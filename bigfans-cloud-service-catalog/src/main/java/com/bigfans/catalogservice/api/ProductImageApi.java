package com.bigfans.catalogservice.api;

import com.bigfans.catalogservice.model.ImageGroup;
import com.bigfans.catalogservice.service.product.ProductImageService;
import com.bigfans.framework.web.BaseController;
import com.bigfans.framework.web.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author lichong
 * @create 2018-03-14 下午7:51
 **/
@RestController
public class ProductImageApi extends BaseController{

    @Autowired
    private ProductImageService productImageService;

    @GetMapping(value = "/images")
    public RestResponse listImages(@RequestParam(value = "prodId") String prodId) throws Exception {
        List<ImageGroup> imageGroups = productImageService.listProductImages(prodId);
        return RestResponse.ok(imageGroups);
    }
}
