package com.bigfans.catalogservice.service.attribute;

import com.bigfans.catalogservice.model.AttributeOption;
import com.bigfans.framework.dao.BaseService;

import java.util.List;

public interface AttributeOptionService extends BaseService<AttributeOption> {

    List<AttributeOption> listByCategory(String catId) throws Exception;

}
