package com.bigfans.model.event.notification;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author lichong
 * @create 2018-02-17 上午10:43
 **/
@Data
@NoArgsConstructor
public class RequestVCodeEvent {

    private String mobile;
    private String code;

    public RequestVCodeEvent(String mobile , String code) {
        this.mobile = mobile;
        this.code = code;
    }
}
