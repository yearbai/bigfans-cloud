package com.bigfans.model.event.order;

import com.bigfans.framework.event.AbstractEvent;
import lombok.Data;

/**
 * @author lichong
 * @create 2018-02-17 下午2:19
 **/
@Data
public class OrderCreatedEvent extends AbstractEvent{

    public OrderCreatedEvent() {
    }

    private String orderId;

    public OrderCreatedEvent(String orderId) {
        this.orderId = orderId;
    }
}
