package com.bigfans.searchservice.model;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class Order {

    protected String id;
    protected Date createDate;
    protected Date updateDate;
    protected Integer deleted = 0;

    // 订单信息
    protected String sn;
    // 订单状态
    protected String status;
    // 取消原因
    protected String cancelReason;
    // 用户信息
    protected String userId;
    // 支付信息
    protected String paymentId;
    // 支付类型ID
    protected String payMethodCode;
    // 支付类型名称
    protected String payMethodName;
    // 收货地址
    protected String addressId;
    // 收货人
    protected String addressConsignee;
    // 送货详细地址
    protected String addressDetail;
    // 收货人电话
    protected String addressPhone;
    // 收货人email
    protected String addressEmail;
    // 留言
    protected String note;
    // 运费
    protected BigDecimal freight;
    // 商品总价格
    protected BigDecimal prodTotalPrice;
    // 总价格(计算完邮费和各种优惠后的应付款额)
    protected BigDecimal totalPrice;
    // 是否使用积分
    protected Boolean isPointUsed;
    // 获得积分
    protected Float gainedPoint;
    // 使用积分
    protected Float usedPoint;
    // 积分减免金额
    protected BigDecimal pointDeductionTotal;
    // 优惠劵信息
    protected Boolean isCouponUsed;
    protected String usedCouponId;
    protected String usedCouponMsg;
    // 优惠劵减免金额
    protected BigDecimal couponDeductionTotal;
    // 余额
    protected Boolean isBalanceUsed;
    // 余额减免金额
    protected BigDecimal balanceDeductionTotal;
    protected BigDecimal usedBalance;
}
