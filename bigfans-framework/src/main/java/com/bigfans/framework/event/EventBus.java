package com.bigfans.framework.event;

/**
 * @author lichong
 * @create 2018-02-10 上午7:23
 **/
public interface EventBus {

    <E> void publishEvent(E event);

}
