package com.bigfans.userservice.dao;

import com.bigfans.framework.dao.BaseDAO;
import com.bigfans.userservice.model.Address;

import java.util.List;

public interface AddressDAO  extends BaseDAO<Address> {

	Address getByUser(String addressId , String userId);
	
	List<Address> listByUser(String userId);
}
