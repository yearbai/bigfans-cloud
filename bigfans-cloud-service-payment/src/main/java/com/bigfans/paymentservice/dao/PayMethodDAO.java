package com.bigfans.paymentservice.dao;

import com.bigfans.framework.dao.BaseDAO;
import com.bigfans.paymentservice.model.PayMethod;

import java.util.List;

/**
 * 
 * @Description:
 * @author lichong
 * 2015年4月4日下午9:27:43
 *
 */
public interface PayMethodDAO extends BaseDAO<PayMethod> {

	PayMethod getByCode(String code);
	
	List<PayMethod> listTopMethods() ;
	
	List<PayMethod> listByType(String type) ;
	
}
